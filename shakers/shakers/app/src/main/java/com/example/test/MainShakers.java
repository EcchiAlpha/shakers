package com.example.test;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class MainShakers extends Activity implements ShakersListener {
	private static final String TAG = MainShakers.class.getSimpleName();
	private static final boolean DEBUG = true;
	private static final int SHORT_DELAY = 500;
	int counter;
	int hs=0;
	private Button button;
	private Button button2;
	private TextView textView;
	private TextView textView2;
	private TextView textView3;
	File filename = new File("/dataHighscore");





	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textView = (TextView) findViewById(R.id.textView);
		textView2 = (TextView) findViewById(R.id.textView2);
		textView3 = (TextView) findViewById(R.id.textView3);
		button = (Button) findViewById(R.id.button);
		button2 = (Button) findViewById(R.id.button2);

	}

	public void showhighscore(View view){
		String text ="";
		try {
			InputStream is = getAssets().open("dataHighscore.txt");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			text = new String(buffer);

			text = new String(buffer);

			textView3.setText("Dein Highscore ist " + text);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void startCounter(View view){

		stopCounter();
		textView2.setText("" + 0);
		new CountDownTimer(30000, 1000) {

			public void onTick(long millisUntilFinished) {
				textView.setText("seconds remaining: " + millisUntilFinished / 1000);
			}



			public void onFinish() {
				textView.setText("done!");
				ShakersManager.stopListening();

				String nummer = (String) textView2.getText();

				/*

				Aus File lesen, funktioniert, macht jedoch keinen Sinn weil mach nicht ins File schreiben kann (weiter unten schauen)


				String text ="";
				try {
					InputStream is = getAssets().open("dataHighscore.txt");
					int size = is.available();
					byte[] buffer = new byte[size];
					is.read(buffer);
					is.close();
					text = new String(buffer);

					text = new String(buffer);

					textView3.setText("Dein Highscore ist " + text);

				} catch (IOException e) {
					e.printStackTrace();
				}*/
				//Log.d(TAG,"highscore ist " + text);
				int eins = Integer.parseInt(nummer);
				//int zwei = Integer.parseInt(text);
				String k = textView2.getText().toString();
				int zwei = Integer.parseInt(k);
				if(eins>=zwei){
					hs=zwei;
					Log.d(TAG,"highscore ist " + hs);
					textView3.setText("Dein Highscore ist " + hs);
					int highScore = Integer.parseInt(nummer);



					/*
					In asset File schreiben, erweiterbar aber zu wenig Zeit in diesen 2 Tagen

					byte[] highScoreBytes = new byte[0];

					highScoreBytes[0] = (byte) highScore;


					FileOutputStream outputStream = null;
					try {
						outputStream = getApplicationContext().openFileOutput("datahighscore", Context.MODE_PRIVATE);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					try {
						outputStream.write(highScoreBytes);
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						outputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}*/
				}
			}
		}.start();
	}

	public void stopCounter(){
		new CountDownTimer(30000, 1000) {

			public void onTick(long millisUntilFinished) {
				textView.setText("seconds remaining: " + millisUntilFinished / 1000);
			}

			public void onFinish() {
				textView.setText("done!");
				ShakersManager.stopListening();
				counter=0;
			}
		}.cancel();
	}



	public void onAccelerationChanged(float x, float y, float z) {

	}

	public void onShake(float force) {
	if(textView.getText().equals("done!")|textView.getText().equals("30")) {
	}else {

		counter = counter + 1;
		textView2.setText("" + counter);

	}
	}

	@Override
    public void onResume() {
            super.onResume();
            if (ShakersManager.isSupported(this)) {

    			ShakersManager.startListening(this);
            }
    }
	
	@Override
    public void onStop() {
            super.onStop();
			stopCounter();
            if (ShakersManager.isListening()) {

    			ShakersManager.stopListening();

            }
           
    }
	
	@Override
	public void onDestroy() {
		super.onDestroy();

		if (ShakersManager.isListening()) {

			ShakersManager.stopListening();

        }
			
	}

}
